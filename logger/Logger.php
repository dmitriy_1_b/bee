<?php

namespace Bee\Logger;

/**
 * Description of Logger
 *
 * @author root
 */
class Logger {

    const WARN_FILE_PATH = '/logs/warn.log';
    const MESSAGE_WARN_FORMAT = "WARNNIG: %s, TRACE:%s\r\n";
    const NOTICE_FILE_PATH = '/logs/notice.log';
    const MESSAGE_NOTICE_FORMAT = "NOTICE: %s, TRACE:%s\r\n";
    const ERROR_FILE_PATH = '/logs/error.log';
    const MESSAGE_ERROR_FORMAT = "ERROR: %s, TRACE:%s\r\n";

    /**
     *
     * @var Logger 
     */
    private static $instance;

    /**
     * 
     * @return Logger
     */
    public static function getInstance() {

        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    private function __construct() {
        
    }

    public function notice($text, $trace) {
        $this->saveToFile(self::NOTICE_FILE_PATH,
                sprintf(self::MESSAGE_NOTICE_FORMAT, $text, $trace));
    }

    public function warn($text, $trace) {
        $this->saveToFile(self::WARN_FILE_PATH,
                sprintf(self::MESSAGE_WARN_FORMAT, $text, $trace));
    }

    public function error($text, $trace) {
        $this->saveToFile(self::ERROR_FILE_PATH,
                sprintf(self::MESSAGE_ERROR_FORMAT, $text, $trace));
    }

    private function saveToFile($file, $data) {
        file_put_contents(SITE_PATH . $file, $data, FILE_APPEND);
    }

}
