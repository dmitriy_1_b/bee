<?php

namespace Bee\Models;

use Bee\Entities\User;
use Bee\Managers\DatabaseManager;
use Bee\Exception\NullPointerException;

class UserModel {
    
    const SELECT_ALL = "SELECT * FROM `users`";
    const FIND_USER_BY_LOGIN = "SELECT * FROM users WHERE `login` = ?";
    const USER_ID = 'id';
    const LOGIN = 'login';
    const PASSWORD = 'password';
    const TABLE = 'users';

    /**
     * 
     * @param int $login
     * @return \Bee\Entities\User
     */
    public function find($login) {
        $database = DatabaseManager::getInstance();
        $request = self::FIND_USER_BY_LOGIN;
        $params = array();
        $params[] = $login;
        $array = $database->readData($request, $params);
        if (count($array)) {
            $userId = $array[0][self::USER_ID];
            $login = $array[0][self::LOGIN];
            $password = $array[0][self::PASSWORD];
            $user = new User($userId, $login, $password);
            return $user;
        } else {
            throw new NullPointerException();
        }
    }

}
