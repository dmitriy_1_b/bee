<?php

namespace Bee\Models;

use Bee\Entities\Task;
use Bee\Managers\DatabaseManager;
use Bee\Exception\NullPointerException;

/**
 * Description of TaskModel
 *
 * @author root
 */
class TaskModel {

    const INSERT = "INSERT INTO `tasks` (`id`, `name`, `email`, `text`, `picture_path`, `status`, `edited`) VALUES (NULL, ?, ?, ?, ?, ?, 0)";
    const UPDATE = "UPDATE `tasks` SET `name`=?, `email`=?, `picture_path`=?, `status`=?, edited=edited |(text!=?), `text`=? WHERE `id`=?";
    const UPDATE_WITHOUT_PICTURE = "UPDATE `tasks` SET `name`=?, `email`=?, `status`=?, edited=edited |(text!=?), `text`=? WHERE `id`=?";
    const UPDATE_STATUS = "UPDATE `tasks` SET `status`=? WHERE `id`=?";
    const FIND_BY_ID = "SELECT * FROM `tasks` WHERE `id` = ?";
    const SELECT_ALL = "SELECT * FROM `tasks`";
    const DELETE_ROW = "DELETE FROM `tasks` WHERE `id` = ?";
    const TABLE = 'tasks';
    const FIELD_ID = 'id';
    const FIELD_NAME = 'name';
    const FIELD_EMAIL = 'email';
    const FIELD_TEXT = 'text';
    const FIELD_PICTURE_PATH = 'picture_path';
    const FIELD_STATUS = 'status';
    const FIELD_EDITED = 'edited';

    /**
     * 
     * @param \Bee\Entities\Task $task
     * @return Integer|Boolean
     */
    public function insert(Task $task) {
        $database = DatabaseManager::getInstance();
        $request = self::INSERT;
        $params = array();
        $params[] = $task->getName();
        $params[] = $task->getEmail();
        $params[] = $task->getText();
        $params[] = $task->getPicturePath();
        $params[] = $task->getStatus();
        $id = $database->insertRow($request, $params);
        return $id;
    }

    /**
     * 
     * @param \Bee\Entities\Task $task
     * @return Integer|Boolean
     */
    public function update(Task $task) {
        $database = DatabaseManager::getInstance();
        $request = self::UPDATE;
        $params = array();
        $params[] = $task->getName();
        $params[] = $task->getEmail();
        $params[] = $task->getPicturePath();
        $params[] = $task->getStatus();
        $params[] = $task->getText();
        $params[] = $task->getText();
        $params[] = $task->getId();
        //If all values equal previous 
        if (!$database->updateDeleteRow($request, $params)) {
            try {
                $this->find($task->getId());
            } catch (NullPointerException $e) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * 
     * @param \Bee\Entities\Task $task
     * @return Integer|Boolean
     */
    public function updateStatus(Task $task) {
        $database = DatabaseManager::getInstance();
        $request = self::UPDATE_STATUS;
        $params = array();
        $params[] = $task->getStatus();
        $params[] = $task->getId();
        //If all values equal previous 
        if (!$database->updateDeleteRow($request, $params)) {
            try {
                $this->find($task->getId());
            } catch (NullPointerException $e) {
                return false;
            }
        }
        return true;
    }

    /**
     * 
     * @param \Bee\Entities\Task $task
     * @return Integer|Boolean
     */
    public function updateWithOutPicture(Task $task) {
        $database = DatabaseManager::getInstance();
        $request = self::UPDATE_WITHOUT_PICTURE;
        $params = array();
        $params[] = $task->getName();
        $params[] = $task->getEmail();
        $params[] = $task->getStatus();
        $params[] = $task->getText();
        $params[] = $task->getText();
        $params[] = $task->getId();
        //If all values equal previous 
        if (!$database->updateDeleteRow($request, $params)) {
            try {
                $this->find($task->getId());
            } catch (NullPointerException $e) {
                return false;
            }
        }
        return true;
    }

    /**
     * 
     * @param int $id
     * @return \Bee\Entities\Task
     */
    public function find($id) {
        $database = DatabaseManager::getInstance();
        $request = self::FIND_BY_ID;
        $params = array();
        $params[] = $id;
        $array = $database->readData($request, $params);
        if (count($array)) {
            $row = $array[0];
            $task = new Task($row[self::FIELD_ID], $row[self::FIELD_NAME],
                    $row[self::FIELD_EMAIL], $row[self::FIELD_TEXT],
                    $row[self::FIELD_PICTURE_PATH], $row[self::FIELD_STATUS], 
                    $row[self::FIELD_EDITED]);
            return $task;
        } else {
            throw new NullPointerException();
        }
    }

    /**
     * @param int $id 
     * @return boolean
     */
    public function delete($id) {
        $database = DatabaseManager::getInstance();
        $params = array();
        $params[] = $id;
        return $database->updateDeleteRow(self::DELETE_ROW, $params);
    }

    /**
     * 
     * @return array an array of \Bee\Entities\Task
     */
    public function getAll() {
        $database = DatabaseManager::getInstance();
        $request = self::SELECT_ALL;
        $params = array();
        $rows = $database->readData($request, $params);
        $tasks = array();
        foreach ($rows as $row) {
            $tasks[] = new Task($row[self::FIELD_ID], $row[self::FIELD_NAME],
                    $row[self::FIELD_EMAIL], $row[self::FIELD_TEXT],
                    $row[self::FIELD_PICTURE_PATH], $row[self::FIELD_STATUS], 
                    $row[self::FIELD_EDITED]);
        }
        return $tasks;
    }

}
