<!doctype html>
<html lang="en">
    <head>
        <?php include 'modules/head.php'; ?>
    </head>
    <body>
        <?php include 'modules/navbar.php'; ?>
        <?php include 'modules/simpleModal.php'; ?>
        <?php include 'modules/scripts.php'; ?>
        <div class="container-fluid">
            <br>
            <div class="row" >   
                <?php include 'modules/login.php'; ?>   
                <?php include 'modules/addTask.php'; ?>  
            </div>
        </div>
    </body>
</html>