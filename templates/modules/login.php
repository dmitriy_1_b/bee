<div class="col-md-2">
    <?php    
    if (!$GLOBALS["isAuth"]):
        ?>
        <form action="<?php echo FORM_SELF; ?>" method="post">
            <p><?php echo $text_enter_login_password; ?></p>
            <input type="hidden" name="route" value="Login/login"/> 
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="<?php echo $text_login; ?>"
                       aria-label="<?php echo $text_login; ?>"  name="login">
            </div>
            <div class="input-group mb-3">
                <input type="password" class="form-control" placeholder="<?php echo $text_password; ?>"
                       aria-label="<?php echo $text_password; ?>" name="password">
            </div>
            <input class="btn btn-secondary" TYPE="submit" VALUE="<?php echo $text_enter; ?>"/><br/> 
        </form>
<?php else: ?>
        <form action="<?php echo FORM_SELF; ?>" method="post">
            <p><?php echo $text_to_login; ?></p>
            <input type="hidden" name="route" value="Login/out"/> 
            <input class="btn btn-secondary" TYPE="submit" VALUE="<?php echo $text_out; ?>"/><br/> 
        </form>
<?php endif ?> 
</div>
