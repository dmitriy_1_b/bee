<div class="col-md-10">
    <table id="taskList" class="display" >
        <thead>
            <tr>
                <th><?php echo $text_id; ?></th>
                <th><?php echo $text_name; ?></th>
                <th><?php echo $text_email; ?></th>
                <th><?php echo $text_text; ?></th>
                <th><?php echo $text_image; ?></th>
                <th><?php echo $text_status; ?></th>
                <th><?php echo $text_action; ?></th>
                <th><?php echo $text_delete; ?></th>
            </tr>
        </thead>
        <tbody>  
        </tbody>
    </table>    
</div>
<div class="d-none" id="task-list-source"><?php 'http://' . HTTP_HOST; ?>/index.php?route=Task/tasksList</div>
<script type='text/javascript' src='<?php echo 'http://' . HTTP_HOST . '/'; ?>templates/js/task_list.js'></script>
