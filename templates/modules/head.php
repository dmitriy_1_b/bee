<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">

<link href="<?php echo 'http://' . HTTP_HOST; ?>/templates/css/main.css" rel="stylesheet" type="text/css" >
<title><?php echo $text_title; ?></title>