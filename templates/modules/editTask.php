<div class="col-md-10">

    <form action="<?php echo FORM_SELF; ?>" method="post" enctype="multipart/form-data">  
        <table id="task" class="display">
            <thead>
                <tr>
                    <th><?php echo $text_id; ?></th>
                    <th><?php echo $text_name; ?></th>
                    <th><?php echo $text_email; ?></th>
                    <th><?php echo $text_text; ?></th>
                    <th><?php echo $text_image; ?></th>
                    <th><?php echo $text_status; ?></th>
                </tr>
            </thead>
            <tbody> 
                <tr>                  
            <input id="send-id" type="hidden" class="form-control" name="id" value="<?php echo $GLOBALS["task"]->getId(); ?>">                    
            <input type="hidden" class="form-control" name="route" value="Task/update">
            <td><?php echo $GLOBALS["task"]->getId(); ?></td>
            <td><input type="text" class="form-control" name="name" value="<?php echo $GLOBALS["task"]->getName(); ?>"></td>
            <td><input type="text" class="form-control" name="email" value="<?php echo $GLOBALS["task"]->getEmail(); ?>"></td>
            <td><textarea  type="text" class="form-control" name="text"><?php echo $GLOBALS["task"]->getText(); ?></textarea></td>
            <td><?php include 'editAddTaskImg.php'; ?></td>                
            <td><input id="send-status" type="checkbox" class="form-control" name="status" value="1" <?php
                if ($GLOBALS["task"]->getStatus()) {
                    echo 'checked';
                }
                ?>></td>
            </tr>
            </tbody>
        </table>  
        <br>
        <input class="btn btn-secondary" TYPE="submit" VALUE="<?php echo $text_save; ?>"/><br> 
    </form>
</div>
<script type='text/javascript' src='<?php echo 'http://' . HTTP_HOST . '/'; ?>templates/js/edit_add_task.js'></script>
