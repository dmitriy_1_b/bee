<?php include 'preview.php'; ?>
<div class="col-md-10">

    <form action="<?php echo FORM_SELF; ?>" method="post" enctype="multipart/form-data">  
        <table id="task" class="display">
            <thead>
                <tr>
                    <th><?php echo $text_name; ?></th>
                    <th><?php echo $text_email; ?></th>
                    <th><?php echo $text_text; ?></th>
                    <th><?php echo $text_image; ?></th>
                </tr>
            </thead>
            <tbody> 
                <tr>                                   
            <input type="hidden" class="form-control" name="route" value="Task/add">
            <td><input id="add-name" type="text" class="form-control" name="name" value=""></td>
            <td><input id="add-email" type="text" class="form-control" name="email" value=""></td>
            <td><textarea  id="add-text" type="text" class="form-control" name="text"></textarea></td>
            <td><?php include 'editAddTaskImg.php'; ?></td>
            </tr>
            </tbody>
        </table>  
        <br>
        <input class="btn btn-secondary" type="submit" value="<?php echo $text_save; ?>"/>
        <button class ="btn btn-secondary" id="preview-button" type="button"><?php echo $text_preview; ?></button><br> 
    </form>
</div>
<script type='text/javascript' src='<?php echo 'http://' . HTTP_HOST . '/'; ?>templates/js/edit_add_task.js'></script>
