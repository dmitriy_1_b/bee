<!-- modal content -->
<div id="alert-preview-content">
    <table id="preview-task" class="display" >
        <thead>
            <tr>
                <th><?php echo $text_id; ?></th>
                <th><?php echo $text_name; ?></th>
                <th><?php echo $text_email; ?></th>
                <th><?php echo $text_text; ?></th>
                <th><?php echo $text_image; ?></th>
                <th><?php echo $text_status; ?></th>
            </tr>
        </thead>
        <tbody>  
        </tbody>
    </table>    
    <br>
    <br>
    <br>
    <h3><input class="btn btn-secondary simplemodal-close" type="submit" value="Ok" id="modal-close"></h3>
</div>
<div class="d-none" id="not-approved-text"><?php echo $text_not_approved; ?></div>
<script type='text/javascript' src='<?php echo 'http://' . HTTP_HOST . '/'; ?>templates/js/preview.js'></script>