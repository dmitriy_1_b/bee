<div class="task-image-container" >
    <img class="task-image" id="img-file"  src="<?php
    //used by add too
    if (isset($GLOBALS["task"])) {
        echo $GLOBALS["task"]->getPicturePath();
    }
    ?>"></img>
</div>    
<br>
<br>
<div class="input-group mb-1">
    <div class="custom-file">
        <input type="file" class="custom-file-input" id="file-selector"  accept="image/png, image/gif, image/jpeg">
        <input type="hidden" id="img-data" name="image_data">
        <input type="hidden" id="img-name" name="image_name">
        <label class="custom-file-label" for="file-selector">Choose file</label>
    </div>
</div>
<script type='text/javascript' src='<?php echo 'http://' . HTTP_HOST . '/'; ?>templates/js/edit_add_task_img.js'></script>

