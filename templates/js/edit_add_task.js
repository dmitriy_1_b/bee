$(document).ready(function () {
    $('#task').DataTable({
        "lengthMenu": [1],
        "lengthChange": false,
        "searching": false,
        "paging": false,
        "info": false
    });
    $('#send-status').click(function () {
        var status = $('#send-status').prop("checked");
        var id = $('#send-id').val();
        if (status) {
            $.ajax({url: "index.php?route=Task/updateStatus&id=" + id + "&status=1", async: false, type: "POST"});
        } else {
            $.ajax({url: "index.php?route=Task/updateStatus&id=" + id, async: false, type: "POST"});
        }
    });
});

