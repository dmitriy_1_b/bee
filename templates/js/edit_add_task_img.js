$(document).ready(function () {
    $('#file-selector').change(function (event) {
        const files = event.target.files;
        const w = 320;
        const h = 240;
        if (files && files.length) {
            var fr = new FileReader();
            fr.onload = function () {
                var canvas = document.createElement('canvas');
                var ctx = canvas.getContext("2d");
                var img = new Image();
                img.onload = function () {
                    var height = w * (img.height / img.width);
                    var width = w;
                    if (height > h) {
                        width = h * (img.width / img.height);
                        height = h;
                    }
                    canvas.width = width;
                    canvas.height = height;
                    ctx.drawImage(img, 0, 0, width, height);
                    var dataUrl = canvas.toDataURL(files[0].type);
                    $('#img-file').attr("src", dataUrl);
                    $("#img-data").val(dataUrl);
                    $("#img-name").val(files[0].name);
                    $("#file-selector").val(null);
                    canvas.remove();
                };
                img.src = fr.result;
            };
            fr.readAsDataURL(files[0]);
        }
    });
});
