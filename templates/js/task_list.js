$(document).ready(function () {
    $('#taskList').DataTable({
        "lengthMenu": [3],
        "searching": false,
        "lengthChange": false,
        "ajax": $('#task-list-source').html()
    });
});