$(document).ready(function () {
    var initContent = $('#alert-preview-content').html();
    var table;
    $('#preview-button').click(function () {
        table = $('#preview-task').DataTable({
            "lengthMenu": [3],
            "searching": false,
            "lengthChange": false,
            "info": false,
            "paging": false,
            data: [[1, $('#add-name').val(), $('#add-email').val(), $('#add-text').val(),
                    '<div class="task-image-container"><img class="task-image" src="' +
                            $('#img-file').attr("src") + '" ></img></div>',
                    '<span class="badge badge-warning">' +
                            $('#not-approved-text').html() + '</span>']]
        });
        $('#alert-preview-content').modal();
        $('#simplemodal-container').height(400);
        $('#simplemodal-container').width(900);
    });
    $('#modal-close').click(function () {
        table.destroy();
        $('#alert-preview-content').html(initContent);
    });
});