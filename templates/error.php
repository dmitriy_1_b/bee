<!doctype html>
<html lang="en">
    <head>
        <?php include 'modules/head.php'; ?>
    </head>
    <body>
        <?php include 'modules/navbar.php'; ?>
        <?php include 'modules/simpleModal.php'; ?>
        <?php include 'modules/scripts.php'; ?>
        <div class="container-fluid">
            <br>
            <br>
            <br>
            <h1 class="text-center"><?php echo $text_error; ?></h1>
        </div>
    </body>
</html>