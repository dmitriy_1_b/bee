<?php

namespace Bee\Controllers;

use Bee\Managers\TextManager;
use Bee\Managers\MessageManager;

Class Index Extends Base {    

    function index() {
        $this->registry['template']->show('index');
    }

    function error() {        
        $this->addMessage(MessageManager::MESSAGE_ERROR);
        $this->registry['template']->show('error');
    }

}
