<?php

namespace Bee\Controllers;

use Bee\Managers\TextManager;
use Bee\Managers\MessageManager;
use Bee\Managers\SessionManager;
use Bee\Controllers\Login;
use Bee\Entities\User;
use Bee\Exception\NullPointerException;

Abstract Class Base {

    protected $registry;
    protected $user;

    function __construct($registry) {
        $this->registry = $registry;
        $isAuth = false;
        try {
            \Bee\Managers\SessionManager::getInstance()->getUser();
            $isAuth = true;
        } catch (\Bee\Exception\NullPointerException $ex) {
            
        }
        //makes it global
        $GLOBALS["isAuth"] = $isAuth;
        //to not do it in template
        if (!isset($GLOBALS["message"])) {
            $GLOBALS["message"] = '';
        }
    }

    protected function userCheck() {
        try {
            $this->user = SessionManager::getInstance()->getUser();
        } catch (NullPointerException $e) {
            //Not authorized            
            $this->exitWithMessage(MessageManager::MESSAGE_NOT_AUTHORIZATION);
        }
    }

    protected function addMessage($messageName) {
        $textManager = TextManager::getInstance();
        MessageManager::getInstance()->addMessage($textManager->getProperty($messageName));
        $GLOBALS["message"] = (string) MessageManager::getInstance();
    }

    protected function exitWithMessage($messageName) {
        $this->addMessage($messageName);
        $index = new Index($this->registry);
        $index->index();
        exit();
    }

}
