<?php

namespace Bee\Controllers;

use Bee\Models\TaskModel;
use Bee\Managers\TextManager;
use Bee\Utils\Filter;
use Bee\Managers\MessageManager;
use Bee\Config\Config;
use Bee\Exception\NullPointerException;
use Bee\Utils\Random;

/**
 * Description of Task
 *
 * @author root
 */
class Task extends Base {

    const IMG_TAG = '<div class="task-image-container"><img class="task-image" src="%s" ></img></div>';
    const STATUS_APPROVED_START_TAG = '<span class="badge badge-success">';
    const STATUS_NOT_APPROVED_START_TAG = '<span class="badge badge-warning">';
    const STATUS_END_TAG = '</span>';
    const STATUS_EDITED = ', %s';
    const EDIT_TAG = '<a href="index.php?route=Task/edit&id=%s">%s</a>';
    const DELETE_TAG = '<a href="index.php?route=Task/delete&id=%s">%s</a>';
    const ID = 'id';
    const NAME = 'name';
    const EMAIL = 'email';
    const TEXT = 'text';
    const STATUS = 'status';
    const JSON_DATA = 'data';
    const IMAGE_DATA = 'image_data';
    const IMAGE_NAME = 'image_name';

    public function tasksList() {
        $taskModel = new TaskModel();
        $tasks = $taskModel->getAll();
        $output = array();
        foreach ($tasks as $task) {
            $statusText;
            if ($task->getStatus()) {
                $statusText = self::STATUS_APPROVED_START_TAG .
                        TextManager::getInstance()->getProperty(TextManager::TEXT_APPROVED);
            } else {
                $statusText = self::STATUS_NOT_APPROVED_START_TAG .
                        TextManager::getInstance()->getProperty(TextManager::TEXT_NOT_APPROVED);
            }
            if ($task->getEdited()) {
                $statusText .= sprintf(self::STATUS_EDITED,
                        TextManager::getInstance()->getProperty(TextManager::TEXT_EDITED));
            }
            $statusText .= self::STATUS_END_TAG;
            $output[] = array($task->getId(), $task->getName(), $task->getEmail(),
                $task->getText(), sprintf(self::IMG_TAG, $task->getPicturePath()),
                $statusText,
                sprintf(self::EDIT_TAG, $task->getId(),
                        TextManager::getInstance()->getProperty(TextManager::TEXT_EDIT)),
                sprintf(self::DELETE_TAG, $task->getId(),
                        TextManager::getInstance()->getProperty(TextManager::TEXT_DELETE)));
        }
        $output = json_encode(array(self::JSON_DATA => $output));
        echo $output;
        exit();
    }

    public function edit() {
        $this->userCheck();
        $taskModel = new TaskModel();
        $id = Filter::int($_REQUEST[self::ID]);
        try {
            $task = $taskModel->find($id);
        } catch (NullPointerException $e) {
            $this->exitWithMessage(MessageManager::MESSAGE_NO_TASK);
        }
        //makes it global
        $GLOBALS["task"] = $task;
        $this->registry['template']->show('edit');
    }

    public function delete() {
        $this->userCheck();
        $taskModel = new TaskModel();
        $id = Filter::int($_REQUEST[self::ID]);
        if (!$taskModel->delete($id)) {
            $this->exitWithMessage(MessageManager::MESSAGE_NO_TASK);
        }
        $this->exitWithMessage(MessageManager::MESSAGE_TASK_DELETE);
    }

    public function update() {
        $this->userCheck();
        $id = Filter::int($_POST[self::ID]);
        if ($id === false) {
            $this->exitWithMessage(MessageManager::MESSAGE_WRONG_ID);
        }
        $name = Filter::string($_POST[self::NAME]);
        $email = Filter::email($_POST[self::EMAIL]);
        if ($email === false) {
            $this->exitWithMessage(MessageManager::MESSAGE_WRONG_EMAIL);
        }
        $text = Filter::string($_POST[self::TEXT]);
        $status = \Bee\Entities\Task::STATUS_NOT_APPROVED;
        if (isset($_POST[self::STATUS])) {
            $status = \Bee\Entities\Task::STATUS_APPROVED;
        }
        $imageName = Filter::fileName($_POST[self::IMAGE_NAME]);
        $res;
        if ($imageName == '') {
            $taskModel = new TaskModel();
            $res = $taskModel->updateWithOutPicture(new \Bee\Entities\Task(
                            $id, $name, $email, $text, null, $status, null));
        } else {
            $imageData = Filter::string($_POST[self::IMAGE_DATA]);
            if ($imageData == '') {
                $this->exitWithMessage(MessageManager::MESSAGE_WRONG_IMAGE);
            }
            $imageData = substr($imageData, strpos($imageData, ",") + 1);
            $imageData = base64_decode($imageData);
            if ($imageData === false) {
                $this->exitWithMessage(MessageManager::MESSAGE_WRONG_IMAGE);
            }
            if (strlen($imageData) > Config::MAX_IMG_SIZE) {
                $this->exitWithMessage(MessageManager::MESSAGE_TOO_BIG_IMAGE);
            }
            $taskModel = new TaskModel();
            file_put_contents(SITE_PATH . Config::TASK_IMGS_PATH . $imageName, $imageData);
            $res = $taskModel->update(new \Bee\Entities\Task(
                            $id, $name, $email, $text,
                            Config::TASK_IMGS_PATH . $imageName, $status, null));
        }
        if ($res === false) {
            $this->exitWithMessage(MessageManager::MESSAGE_WRONG_ID);
        }
        $this->exitWithMessage(MessageManager::MESSAGE_TASK_UPDATE);
    }

    public function add() {
        $name = Filter::string($_POST[self::NAME]);
        $email = Filter::email($_POST[self::EMAIL]);
        if ($email === false) {
            $this->exitWithMessage(MessageManager::MESSAGE_WRONG_EMAIL);
        }
        $text = Filter::string($_POST[self::TEXT]);
        $status = \Bee\Entities\Task::STATUS_NOT_APPROVED;
        $imageName = Filter::fileName($_POST[self::IMAGE_NAME]);
        if ($imageName == '') {
            $this->exitWithMessage(MessageManager::MESSAGE_WRONG_IMAGE);
        }
        $imageData = Filter::string($_POST[self::IMAGE_DATA]);
        if ($imageData == '') {
            $this->exitWithMessage(MessageManager::MESSAGE_WRONG_IMAGE);
        }
        $imageData = substr($imageData, strpos($imageData, ",") + 1);
        $imageData = base64_decode($imageData);
        if ($imageData === false) {
            $this->exitWithMessage(MessageManager::MESSAGE_WRONG_IMAGE);
        }
        if (strlen($imageData) > Config::MAX_IMG_SIZE) {
            $this->exitWithMessage(MessageManager::MESSAGE_TOO_BIG_IMAGE);
        }
        if (file_exists(SITE_PATH . Config::TASK_IMGS_PATH . $imageName)) {
            $rand = new Random();
            $imageName = $rand->string() . $imageName;
        }
        file_put_contents(SITE_PATH . Config::TASK_IMGS_PATH . $imageName, $imageData);

        $taskModel = new TaskModel();
        $res = $taskModel->insert(new \Bee\Entities\Task(
                        null, $name, $email, $text,
                        Config::TASK_IMGS_PATH . $imageName, $status, null));

        if ($res === false) {
            $this->exitWithMessage(MessageManager::MESSAGE_WRONG_ID);
        }
        $this->exitWithMessage(MessageManager::MESSAGE_TASK_ADD);
    }

    public function addForm() {
        $this->registry['template']->show('add');
    }

    public function updateStatus() {
        $this->userCheck();
        $id = Filter::int($_GET[self::ID]);
        $status = \Bee\Entities\Task::STATUS_NOT_APPROVED;
        if (isset($_GET[self::STATUS])) {
            $status = \Bee\Entities\Task::STATUS_APPROVED;
        }
        $taskModel = new TaskModel();
        $taskModel->updateStatus(new \Bee\Entities\Task(
                        $id, null, null, null,
                        null, $status, null));
        exit();
    }

}
