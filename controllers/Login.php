<?php

namespace Bee\Controllers;

use Bee\Managers\TextManager;
use Bee\Utils\Filter;
use Bee\Entities\User;
use Bee\Models\UserModel;
use Bee\Utils\PasswordHash;
use Bee\Managers\SessionManager;
use Bee\Managers\MessageManager as Ms;
use Bee\Exception\NullPointerException;

Class Login Extends Base {

    const PASSWORD = 'password';
    const LOGIN = 'login';

    public function login() {
        $ms = Ms::getInstance();
        $tm = TextManager::getInstance();
        $login = Filter::string($_POST[self::LOGIN]);
        $password = Filter::string($_POST[self::PASSWORD]);
        if ($login == '' or $password == '') {
            $this->exitWithMessage(Ms::MESSAGE_EMPTY_LOGIN_PASSWORD);
        }
        $userModel = new UserModel();
        try{
        $user = $userModel->find($login);
        }catch(NullPointerException $e){
            $this->exitWithMessage(Ms::MESSAGE_WRONG_LOGIN_PASSWORD);
        }
        $passHash = PasswordHash::getHash($password);
        if ($passHash == $user->getPassHash()) {
            SessionManager::getInstance()->setUser($user);
            $GLOBALS["isAuth"] = true;
            $index = new Index($this->registry);
            $index->index();
        } else {
           $this->exitWithMessage(Ms::MESSAGE_WRONG_LOGIN_PASSWORD);
        }
    }

    private function index() {
        $index = new Index($this->registry);
        $index->index();
    }

    public function out() {
        SessionManager::getInstance()->unsetUser();
        $GLOBALS["isAuth"] = false;
        $this->index();
    }

}
