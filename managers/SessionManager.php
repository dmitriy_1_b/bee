<?php

namespace Bee\Managers;

use Bee\Entities\User;
use Bee\Exception\NullPointerException;

class SessionManager {

    const USER = 'user';
    const USER_ID = 'user_id';
    const LOGIN = 'login';
    const PASSWORD = 'password';    
    
    /**
     *
     * @var SessionManager 
     */
    private static $instance;
    
    /**
     * 
     * @return SessionManager
     */
    public static function getInstance() {

        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    private function __construct() {
    }

    /**
     * 
     * @return \Bee\Entities\User
     */
    public function getUser() {
        if (isset($_SESSION[self::USER])) {
            $arr = $_SESSION[self::USER];
            return new User(
                    $arr[self::USER_ID], $arr[self::LOGIN], $arr[self::PASSWORD]);
        } else {
            throw new NullPointerException();
        }
    }

    /**
     * 
     * @param \Bee\Entities\User $user
     */
    public function setUser(User $user) {

        $_SESSION[self::USER] = array(
            self::USER_ID => $user->getId(),
            self::LOGIN => $user->getLogin(),
            self::PASSWORD => $user->getPassHash()
        );
    }

    /**
     * 
     * @param \Bee\Entities\User $user
     */
    public function unsetUser() {
        if (isset($_SESSION[self::USER])) {
            unset($_SESSION[self::USER]);
        }
        session_destroy();
    }
    
    public function setVar($name,$var) {
        $_SESSION[$name] = $var;
    }

    public function getVar($name) {
        if (isset($_SESSION[$name])) {
            return $_SESSION[$name];
        } else {
            throw new NullPointerException();
        }
    }

}
