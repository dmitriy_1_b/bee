<?php

namespace Bee\Managers;
/**
 * MessageManager display messages to user
 */
class MessageManager {

    const MESSAGE_EMPTY_FIELDS = 'message_empty_fields';
    const MESSAGE_EMPTY_FIELD = 'message_empty_field';
    const MESSAGE_END = 'message_end';
    const MESSAGE_NOT_AUTHORIZATION = 'message_not_authorization';
    const MESSAGE_CHOSE_VALUE = 'message_chose_value';
    const MESSAGE_ENTER_VALUE = 'message_enter_value';
    const MESSAGE_WRONG_LOGIN_PASSWORD = 'message_wrong_login_password';
    const MESSAGE_EMPTY_LOGIN_PASSWORD = 'message_empty_login_password';
    const MESSAGE_USER_REQISTERED = 'message_user_reqistered';
    const MESSAGE_USER_ALREADY_REQISTERED = 'message_user_already_reqistered';
    const MESSAGE_NOT_RIGHTS = 'message_not_rights';
    const MESSAGE_ERROR = 'message_error';
    const MESSAGE_NO_TASK = 'message_no_task';
    const MESSAGE_WRONG_EMAIL = 'message_wrong_email';
    const MESSAGE_WRONG_ID = 'message_wrong_id';
    const MESSAGE_TOO_BIG_IMAGE = 'message_too_big_image';
    const MESSAGE_WRONG_IMAGE = 'message_wrong_image';
    const MESSAGE_TASK_UPDATE = 'message_task_update';
    const MESSAGE_TASK_DELETE = 'message_task_delete';
    const MESSAGE_TASK_ADD = 'message_task_add';

    /**
     *
     * @var MessageManager 
     */
    private static $instance;
    
    /**
     * 
     * @return MessageManager
     */
    public static function getInstance() {

        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    private function __construct() {
        $this->messageArray = array();
    }

    /**
     * 
     * @param string $message
     */
    public function addMessage($message) {
        $this->messageArray[] = $message;
    }

    /**
     * 
     * @return string
     */
    public function __toString() {
        $string = '';
        foreach ($this->messageArray as $message) {
            $string.=$message;
        }
        return $string;
    }

}
