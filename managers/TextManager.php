<?php

namespace Bee\Managers;

class TextManager {

    const DEFAULT_BUNDLE_NAME = "text_ru_RU.properties";
    const RU_RU_BUNDLE_NAME = "text_ru_RU.properties";
    const PROPERTIES_PATH = "properties/";
    const PART_TEXT = 'text';
    const TEXT_EDIT = 'text_edit';
    const TEXT_DELETE = 'text_delete';
    const TEXT_APPROVED = 'text_approved';
    const TEXT_NOT_APPROVED = 'text_not_approved';
    const TEXT_EDITED = 'text_edited';
    
    /**
     *
     * @var TextManager 
     */
    private static $instance;

    /**
     *
     * @var ResourceBundle 
     */
    private $resourceBundle;

    /**
     * 
     * @return TextManager
     */
    public static function getInstance() {

        if (!isset(self::$instance)) {
            self::$instance = new self();
            self::$instance->resourceBundle = parse_ini_file(SITE_PATH . self::PROPERTIES_PATH . self::DEFAULT_BUNDLE_NAME);
        }
        return self::$instance;
    }

    public function getTextConstants() {
        $constants = array();
        //return only if var name start from text
        foreach ($this->resourceBundle as $key => $value) {
            if (substr($key, 0, 4) == self::PART_TEXT) {
                $constants[$key] = $value;
            }
        }
        return $constants;
    }

    /**
     * 
     * @param String $key
     * @return String
     */
    public function getProperty($key) {
        return self::$instance->resourceBundle[$key];
    }

}
