<?php

namespace Bee\Managers;

class DatabaseManager {

    const DRIVER_FILE = 'config/base.ini';
    const SERVER = 'server';
    const DATABASE = 'database';
    const PASSWORD = 'password';
    const USER = 'user';
    const MYSQL_EXCEPTION = 'MYSQL EXCEPTION:  %s, REQUEST: %s';
    const MYSQL_EXCEPTION_DUPLICATE = 'Duplicate';
    const EXCEPTION_FAILE_TO_OPEN_BASE_INI = 'Faile to open base.ini';
    const EXCEPTION_CANNOT_CONNECT_TO_THE_SERVER = 'Cannot connect to the server:';

    /**
     *
     * @var DatabaseManager 
     */
    private static $instance;

    /**
     *
     * @var \mysqli 
     */
    private $mysqli;

    /**
     * 
     * @return DatabaseManager
     */
    public static function getInstance() {

        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct() {
        $params = parse_ini_file(SITE_PATH . self::DRIVER_FILE);
        if ($params == FALSE) {
            throw new Exception(self::EXCEPTION_FAILE_TO_OPEN_BASE_INI);
        }
        $db = new \mysqli($params[self::SERVER], $params[self::USER], $params[self::PASSWORD], $params[self::DATABASE]);
        if ($db->connect_errno) {
            throw new Exception(self::EXCEPTION_CANNOT_CONNECT_TO_THE_SERVER . $db->connect_error);
        }
        $db->set_charset('utf8');
        $this->mysqli = $db;
    }

    /**
     * 
     * @param string $request
     * @param array $params
     * @return array
     * @throws Exception
     */
    public function readData($request, $params = array()) {
        $stmt = $this->prepare($request, $params);
        $result = $stmt->get_result();
        $array = array();
        while ($row = $result->fetch_assoc()) {
            $array[] = $row;
        }
        $stmt->close();
        return $array;
    }

    /**
     * 
     * @param string $request
     * @param array $params
     * @return Integer|boolean
     * @throws Exception
     */
    public function insertRow($request, $params = array()) {
        $stmt = $this->prepare($request, $params);
        $stmt->close();
        return $this->mysqli->insert_id;
    }

    /**
     * 
     * @param string $request
     * @param array $params
     * @return boolean
     * @throws Exception
     */
    public function updateDeleteRow($request, $params = array()) {
        $stmt = $this->prepare($request, $params);
        $flag = ($stmt->affected_rows > 0) ? true : false;
        $stmt->close();
        return $flag;
    }

    private function refValues($arr) {
        $refs = array();
        foreach ($arr as $key => $value)
            $refs[$key] = &$arr[$key];
        return $refs;
    }

    /**
     * 
     * @param type $request
     * @param type $params
     * @return \mysqli_stmt
     */
    private function prepare(&$request, $params) {
        $stmt = $this->mysqli->prepare($request);
        if (count($params)) {
            $types = '';
            if (is_array($params)) {
                foreach ($params as $param) {
                    if (is_string($param)) {
                        $types .= 's';
                    } elseif (is_integer($param)) {
                        $types .= 'i';
                    } elseif (is_float($param)) {
                        $types .= 'd';
                    } else {
                        $types .= 's';
                    }
                }
            }
            array_unshift($params, $types);
            call_user_func_array(array($stmt, 'bind_param'), $this->refValues($params));
        }
        if ($stmt === false) {
            throw new \Exception(sprintf(self::MYSQL_EXCEPTION, '', $request));
        }
        if (!$stmt->execute()) {
            throw new \Exception(sprintf(self::MYSQL_EXCEPTION, $stmt->error, $request));
        }
        return $stmt;
    }

}
