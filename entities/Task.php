<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Bee\Entities;

/**
 * Description of Task
 *
 * @author root
 */
class Task {   
    
    const STATUS_APPROVED = 1;
    const STATUS_NOT_APPROVED = 0;

    private $id;
    private $name;
    private $email;
    private $text;
    private $picturePath;
    private $status;
    private $edited;

    function __construct($id, $name, $email, $text, $picturePath, $status, $edited) {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->text = $text;
        $this->picturePath = $picturePath;
        $this->status = $status;
        $this->edited = $edited;
    }
    
    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getEmail() {
        return $this->email;
    }

    function getText() {
        return $this->text;
    }

    function getPicturePath() {
        return $this->picturePath;
    }

    function getStatus() {
        return $this->status;
    }

    function getEdited() {
        return $this->edited;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setText($text) {
        $this->text = $text;
    }

    function setPicturePath($picturePath) {
        $this->picturePath = $picturePath;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setEdited($edited) {
        $this->edited = $edited;
    }

}
