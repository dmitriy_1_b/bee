<?php
namespace Bee\Entities;

class User {
    
    private $id;
    
    private $login;
    
    private $passHash;
    
    function __construct($id, $login, $passHash) {
        $this->id = $id;
        $this->login = $login;
        $this->passHash = $passHash;
    }
    
    function getId() {
        return $this->id;
    }

    function getLogin() {
        return $this->login;
    }

    function getPassHash() {
        return $this->passHash;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setPassHash($passHash) {
        $this->passHash = $passHash;
    }
    
}
