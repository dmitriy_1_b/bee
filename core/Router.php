<?php

namespace Bee\Core;

use Bee\Utils\Filter;

Class Router {

    private $registry;
    private $path;

    const DEFAULT_CONTROLLER = 'Index';
    const DEFAULT_ACTION = 'index';
    const CONTROLLERS_PATH = 'controllers/';
    const CONTROLLERS_NAMESPACE =  '\\Bee\\Controllers\\';
    const ERROR_NOT_FOUND = '404 Not Found path: ';
    const ERROR_ACTION_NOT_FOUND = 'Action Not found %s/%s';

    function __construct($registry) {
        $this->registry = $registry;
        $this->path = SITE_PATH . self::CONTROLLERS_PATH;
    }

    function delegate() {
        // Analyze route
        $this->getController($file, $controller, $action, $args);


        // File available?
        if (is_readable($file) == false) {
            throw new \Exception(self::ERROR_NOT_FOUND.$file);
        }

        // Include the file
        require_once ($file);

        // Initiate the class
        $class = self::CONTROLLERS_NAMESPACE . $controller;
        $controller = new $class($this->registry);

        // Action available?
        if (is_callable(array($controller, $action)) == false) {
            throw new \Exception(sprintf(self::ERROR_ACTION_NOT_FOUND,$controller,$controller));
        }

        // Run action
        $controller->$action();
    }

    private function getController(&$file, &$controller, &$action, &$args) {
        $route = (empty($_REQUEST['route'])) ? '' : $_REQUEST['route'];
        $route = Filter::string($route);

        if (empty($route)) {
            $route = self::DEFAULT_CONTROLLER;
        }

        // Get separate parts
        $route = trim($route, '/\\');
        $parts = explode('/', $route);

        // Find right controller
        $cmdPath = $this->path;
        foreach ($parts as $part) {
            $fullPath = $cmdPath . $part;

            // Is there a dir with this path?
            if (is_dir($fullPath)) {
                $cmdPath .= $part . DIRECTORY_SEPARATOR;
                array_shift($parts);
                continue;
            }

            // Find the file
            if (is_file($fullPath . '.php')) {
                $controller = $part;
                array_shift($parts);
                break;
            }
        }

        if (empty($controller)) {
            $controller = self::DEFAULT_CONTROLLER;
        }

        // Get action
        $action = array_shift($parts);
        if (empty($action)) {
            $action = self::DEFAULT_ACTION;
        }

        $file = $cmdPath . $controller . '.php';
        $args = $parts;
    }

}
