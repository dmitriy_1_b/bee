<?php

namespace Bee\Core;

use Bee\Exception\NullPointerException;

class Registry Implements \ArrayAccess {

    const EXCEPTION_UNABLE_TO_SET_VAR = 'Unable to set var `%s`. Already set.';

    private $vars = array();

    function set($key, $var) {
        if (isset($this->vars[$key]) == true) {
            throw new \Exception(sprintf(self::EXCEPTION_UNABLE_TO_SET_VAR, $key));
        }

        $this->vars[$key] = $var;
        return true;
    }

    function get($key) {
        if (isset($this->vars[$key]) == false) {
            throw new NullPointerException();
        }

        return $this->vars[$key];
    }

    function remove($key) {
        unset($this->vars[$key]);
    }

    function offsetExists($offset) {
        return isset($this->vars[$offset]);
    }

    function offsetGet($offset) {
        return $this->get($offset);
    }

    function offsetSet($offset, $value) {
        $this->set($offset, $value);
    }

    function offsetUnset($offset) {
        unset($this->vars[$offset]);
    }

}
