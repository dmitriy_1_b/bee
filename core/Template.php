<?php

namespace Bee\Core;

use Bee\Managers\TextManager;

Class Template {
    
    const BASE_VIEW = 'base';
    const EXCEPTION_UNABLE_TO_SET_VAR = 'Unable to set var `%s`. Already set, and overwrite not allowed.';
    const EXCEPTION_TEMPLATE_NOT_EXIST = 'Template %s does not exist.';
    const TEMPLATES_FOLDER = 'templates';
    
    private $registry;
    private $vars = array();

    function __construct($registry) {
        $this->registry = $registry;
    }

    function set($varname, $value, $overwrite = false) {
        if (isset($this->vars[$varname]) == true AND $overwrite == false) {
            throw new \Exception(sprintf(self::EXCEPTION_UNABLE_TO_SET_VAR, $varname));
        }

        $this->vars[$varname] = $value;
        return true;
    }

    function remove($varname) {
        unset($this->vars[$varname]);
        return true;
    }

    private function loadBaseConst() {
        $textManager = TextManager::getInstance();
        $constants = $textManager->getTextConstants();
        foreach ($constants as $key => $value) {
            $this->vars[$key] = $value;
        }
    }

    function show($name) {
        $path = SITE_PATH . self::TEMPLATES_FOLDER . DIRECTORY_SEPARATOR . $name . '.php';

        if (file_exists($path) == false) {
            throw new \Exception(sprintf(self::EXCEPTION_TEMPLATE_NOT_EXIST, $name));
        }
        $this->loadBaseConst();
        // Load variables
        foreach ($this->vars as $key => $value) {
            $$key = $value;
        }
        include ($path);
    }

}
