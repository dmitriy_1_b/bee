<?php

set_time_limit(0);
session_start();
use Bee\Core\Registry;
use Bee\Core\Template;
use Bee\Core\Router;
use Bee\Logger\Logger;

require_once 'utils/startup.php';
try {
    $registry = new Registry;
// Load template object
    $template = new Template($registry);
    $registry->set('template', $template);
// Load router
    $router = new Router($registry);
    $registry->set('router', $router);
    $router->delegate();
} catch (\Exception $e) {
    Logger::getInstance()->error($e->getMessage() , $e->getTraceAsString());
    $_REQUEST['route'] = 'Index/error';
    $registry = new Registry;
// Load template object
    $template = new Template($registry);
    $registry->set('template', $template);
// Load router
    $router = new Router($registry);
    $registry->set('router', $router);
    $router->delegate();
}