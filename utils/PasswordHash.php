<?php
namespace Bee\Utils;
class PasswordHash {
	
    /** Random string */
    const POSTFIX = 'dfg534fsd554';
    /**
     * 
     * @param String $password
     * @return String Hash
     */
    public static function getHash($password){
        return md5(md5($password).self::POSTFIX); 
    }
}
