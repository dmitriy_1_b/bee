<?php

error_reporting(0); //E_ALL
ini_set('display_errors', 'Off');
if (version_compare(phpversion(), '5.1.0', '<') == true) {
    die('PHP5.1 or better');
}

// Get site path
$site_path = realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
define('SITE_PATH', $site_path);

//protect from php_self xss attack
$phpSelf = htmlentities($_SERVER['PHP_SELF']);
define('FORM_SELF', $phpSelf);
$httpHost = htmlentities($_SERVER['HTTP_HOST']);
define('HTTP_HOST', $httpHost);

spl_autoload_register(function ($class_name) {
    $pos = strpos($class_name, '\\');
    if (($pos !== 0) and ( $pos !== false)) {
        $class_name = substr($class_name, $pos + 1);
        $class_name = str_replace('\\', DIRECTORY_SEPARATOR, $class_name);
        $pos = strrpos($class_name, DIRECTORY_SEPARATOR);
        if (($pos !== 0) and ( $pos !== false)) {
            $path = strtolower(substr($class_name, 0, $pos + 1));
            $name = substr($class_name, $pos + 1);
            $class_name = $path . $name;
        }
        include SITE_PATH . $class_name . '.php';
    }
});
