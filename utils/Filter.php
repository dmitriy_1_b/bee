<?php

namespace Bee\Utils;

class Filter {

    /**
     * 
     * @param Integer $number
     * @param Integer $minRange
     * @param Integer $maxRange
     * @return Boolean|Integer
     */
    public static function int($number, $minRange = 0, $maxRange = PHP_INT_MAX) {
        $options = array(
            'options' => array(
                'min_range' => $minRange,
                'max_range' => $maxRange,
            )
        );
        return filter_var($number, FILTER_VALIDATE_INT, $options);
    }

    /**
     * 
     * @param Float $number
     * @return Boolean|Float
     */
    public static function float($number) {
        return filter_var($number, FILTER_VALIDATE_FLOAT);
    }

    /**
     * 
     * @param String $string
     * @return String
     */
    public static function string($string) {
        $string = trim($string);
        //sql sanitialization made by bind_param procedure 
        $string = filter_var($string, FILTER_SANITIZE_STRING);
        if ($string === false) {
            $string = '';
        }
        return $string;
    }
    
    /**
     * 
     * @param String $string
     * @return String
     */
    public static function fileName($string) {
        $string = trim($string);
        //sql sanitialization made by bind_param procedure 
        $string = filter_var($string, FILTER_SANITIZE_STRING);
        if ($string === false) {
            $string = '';
        }
        $string = str_replace('\\', '', $string);
        $string = str_replace('/', '', $string);
        return $string;
    }
    
     

    /**
     * 
     * @param String $string
     * @return Boolean|String
     */
    public static function email($string) {
        $string = trim($string);
        //sql sanitialization made by bind_param procedure 
        return filter_var($string, FILTER_VALIDATE_EMAIL);
    }

    /**
     * 
     * @param String $date
     * @return Boolean|String
     */
    public static function date($date) {
        $date = trim($date);
        $date = filter_var($date, FILTER_SANITIZE_STRING);
        if ($date != FALSE) {
            $flag = preg_match('/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/', $date);
            if ($flag) {
                return $date;
            }
        }
        return False;
    }

}
